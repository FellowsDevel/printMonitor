package br.fellows.print;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.List;

import br.fellows.print.config.FileToPrinterConfig;
import br.fellows.print.config.ReadConfigJSON;

public class FilePrint {

	@SuppressWarnings( "rawtypes" )
	public static void main( String[] args ) throws Exception {
		ReadConfigJSON<FileToPrinterConfig> jsonFtP = new ReadConfigJSON<FileToPrinterConfig>();

		jsonFtP.parse();
		HashMap<String, FileToPrinterConfig> configurations = jsonFtP.getConfigs();

		WatchService watchService = FileSystems.getDefault().newWatchService();

		if( configurations == null ) {
			System.out.println( "Verifique a exist�ncia do arquivo de configura��o 'preferences.config'\n\n" );
			System.exit( 1 );
		}

		for( FileToPrinterConfig fc : configurations.values() ) {
			Path folderSrc = Paths.get( fc.getFolderSrc() );
//			if( !configurations.containsKey( fc.getFileNameEnding() ) ) {
				configurations.put( fc.getFileNameEnding(), fc );
				folderSrc.register( watchService, ENTRY_CREATE, ENTRY_MODIFY );
//			}
		}

		for( ;; ) {
			WatchKey watchKey = watchService.take();
			List<WatchEvent<?>> events = watchKey.pollEvents();

			for( WatchEvent watchEvent : events ) {

				WatchEvent.Kind kind = watchEvent.kind();

				if( kind == OVERFLOW ) {
					System.out.println( "OVERFLOW!!!!" );
					continue;
				}

				// Nome do evento que aconteceu.
				String eventKindName = kind.name();
				// System.out.println( "Nome do evento: " + eventKindName );

				// Pega o nome do arquivo que disparou o evento
				String fileName = watchEvent.context().toString();
				String fileNameEnding = "";
				
				for(FileToPrinterConfig ftpc : configurations.values()){
					fileNameEnding = ftpc.getFileNameEnding();
					if(fileName.substring(0, fileName.lastIndexOf( "." )).endsWith( fileNameEnding ) ){
						break;
					}
					fileNameEnding = null;
				}
				if (fileNameEnding == null){
					continue;
				}

				// System.out.println( "Arquivo: " + fileName );
				// System.out.println( "pasta: " + watched );
				// System.out.println( " " );

				// Verifica se o arquivo est� na pasta de origem
				File f = new File( configurations.get( fileNameEnding ).getFolderSrc() + fileName );
				if( !f.exists() && !f.isDirectory() ) {
					// System.out.println( "O arquivo <" + fileName + "> n�o se encontra na pasta correta, ignorando..." );
				} else {
					if( eventKindName.equals( "ENTRY_MODIFY" ) ) {
						// executa a a��o
						// System.out.println( "A c�pia do arquivo na pasta de origem terminou, executando a a��o configurada\n" );
						doAction( configurations.get( fileNameEnding ), fileName );
					}
				}
			}
			if( !watchKey.reset() ) {
				break;
			}
		}
		watchService.close();
	}

	private static void doAction( FileToPrinterConfig fc, String fileName ) {

		Path src = Paths.get( fc.getFolderSrc() + fileName );
		Path dst = Paths.get( fc.getFolderDst() );

		DirectPrint dp = new DirectPrint();
		dp.setPrinter( fc.getPrinter());
		dp.printString( fc.getFolderSrc() + fileName );
		
		while( true ) {
			try {
				Files.move( src, dst.resolve( src.getFileName() ), StandardCopyOption.REPLACE_EXISTING );
				break;
			} catch( IOException e ) {
			}
		}
		System.out.println( "Arquivo <" + src.getFileName() + "> movido..." );

	}
}
