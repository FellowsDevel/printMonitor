package br.fellows.print;

import java.awt.*;
import java.awt.print.*;
import java.io.*;
import java.util.Vector;

import javax.print.PrintService;

@SuppressWarnings( { "rawtypes", "unchecked" } )
public class PageableText implements Pageable, Printable {

	private PrintService[]	printService;

	// Constants for font name, size, style and line spacing
	private static String		FONTFAMILY				= Font.MONOSPACED;
	private static int			FONTSIZE					= 13;
	private static int			FONTSTYLE					= Font.PLAIN;
	private static float		LINESPACEFACTOR		= 1.1f;

	private String					_family						= null;
	private int							_style						= -1;
	private int							_fontSize					= -1;
	private float						_lineSpaceFactor	= -1;
	private int							_printer					= 0;

	private PageFormat			format;
	private Vector					lines;
	private Font						font;
	private int							linespacing;
	private int							linesPerPage;
	private int							numPages;
	private int							baseline					= -1;
	private Reader					stream;

	/** Create a PageableText object for a string of text */
	public PageableText( String text ) throws IOException {
		this( new StringReader( text ) );
	}

	/** Create a PageableText object for a file of text */
	public PageableText( File file ) throws IOException {
		this( new FileReader( file ) );
	}

	private void validateParameters() throws IOException {

		if( this._lineSpaceFactor >= 0 ) {
			this._lineSpaceFactor = LINESPACEFACTOR;
		}

		if( this._fontSize == -1 || this._fontSize < 5 ) {
			this._fontSize = FONTSIZE;
		}

		if( this._style == 0 || ( this._style != Font.BOLD && this._style != Font.ITALIC
				&& this._style != Font.PLAIN ) ) {
			this._style = FONTSTYLE;
		}

		if( this._family == null || ( !this._family.equalsIgnoreCase( Font.MONOSPACED )
				&& !this._family.equalsIgnoreCase( Font.SANS_SERIF )
				&& !this._family.equalsIgnoreCase( Font.SERIF ) ) ) {
			this._family = FONTFAMILY;
		}

		// First, read all the text, breaking it into lines.
		// This code ignores tabs and does not wrap long lines.
		BufferedReader in = new BufferedReader( this.stream );
		lines = new Vector();
		String line;
		while( ( line = in.readLine() ) != null ) {
//			System.out.println( line );
			lines.addElement( line );
		}
		in.close();

		// Create the font we will use, and compute spacing between lines
		linespacing = (int) ( _fontSize * _lineSpaceFactor );

		// Figure out how many lines per page and how many pages
		linesPerPage = (int) Math.floor( format.getImageableHeight() / linespacing );
		numPages = ( lines.size() - 1 ) / linesPerPage + 1;

		font = new Font( _family, _style, _fontSize );
	}

	public void setLineSpaceSeparator( float lineSpaceFactor ) {
		this._lineSpaceFactor = lineSpaceFactor;
	}

	public void setPrinter( int printer ) {
		this._printer = printer;
	}

	public void setFontSize( int fontSize ) {
		this._fontSize = fontSize;
	}

	public void setStyle( int style ) {
		this._style = style;
	}

	public void setFamily( String family ) {
		this._family = family;
	}

	public void print() throws NullPointerException, IOException, PrinterException {
		// Get the PrinterJob object that coordinates everything
		PrinterJob job = PrinterJob.getPrinterJob();

		// Get the default page format, then ask the user to customize it
		// PageFormat format = job.pageDialog( job.defaultPage() );
		this.format = job.defaultPage();

		validateParameters();
		// Create our PageableText object, and tell the PrinterJob about it
		// job.setPageable( new PageableText( new File( this.fileName ) ) );
		job.setPageable( this );

		job.setPrintService( printService[ this._printer ] );
		job.print();
	}

	/** Create a PageableText object for a stream of text */
	public PageableText( Reader stream ) throws IOException {
		this.stream = stream;
		this.printService = PrinterJob.lookupPrintServices();
	}

	// These are the methods of the Pageable interface.
	// Note that the getPrintable() method returns this object, which means
	// that this class must also implement the Printable interface.
	public int getNumberOfPages() {
		return numPages;
	}

	public PageFormat getPageFormat( int pagenum ) {
		return format;
	}

	public Printable getPrintable( int pagenum ) {
		return this;
	}

	/**
	 * This is the print() method of the Printable interface.
	 * It does most of the printing work.
	 */
	public int print( Graphics g, PageFormat format, int pagenum ) {
		// Tell the PrinterJob if the page number is not a legal one
		if( ( pagenum < 0 ) | ( pagenum >= numPages ) )
			return NO_SUCH_PAGE;

		// First time we're called, figure out the baseline for our font.
		// We couldn't do this earlier because we needed a Graphics object.
		if( baseline == -1 ) {
			FontMetrics fm = g.getFontMetrics( font );
			baseline = fm.getAscent();
		}

		// Clear the background to white. This shouldn't be necessary but is
		// required on some systems to work around an implementation bug.
		g.setColor( Color.white );
		g.fillRect( (int) format.getImageableX(), (int) format.getImageableY(),
				(int) format.getImageableWidth(), (int) format.getImageableHeight() );

		// Set the font and the color we will be drawing with.
		// Note that you cannot assume that black is the default color!
		g.setFont( font );
		g.setColor( Color.black );

		// Figure out which lines of text we will print on this page
		int startLine = pagenum * linesPerPage;
		int endLine = startLine + linesPerPage - 1;
		if( endLine >= lines.size() )
			endLine = lines.size() - 1;

		// Compute the position on the page of the first line
		int x0 = (int) format.getImageableX();
		int y0 = (int) format.getImageableY() + baseline;

		// Loop through the lines, drawing them all to the page
		for( int i = startLine; i <= endLine; i++ ) {
			// Get the line
			String line = (String) lines.elementAt( i );

			// Draw the line.
			// We use the integer version of drawString(), not the Java 2D
			// version that uses floating-point coordinates. A bug in early
			// Java 1.2 implementations prevents the Java 2D version from working.
			if( line.length() > 0 )
				g.drawString( line, x0, y0 );

			// Move down the page for the next line
			y0 += linespacing;
		}

		// Tell the PrinterJob that we successfully printed the page
		return PAGE_EXISTS;
	}

}
