package br.fellows.print;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class FileMoveThread implements Runnable {

	private Path	src;
	private Path	dst;
	private int		tries	= 100;

	public FileMoveThread( Path src, Path dst ) {
		this.src = src;
		this.dst = dst;
	}

	public int getTries() {
		return tries;
	}

	public void setTries( int tries ) {
		if( tries > 0 ) {
			this.tries = tries;
		}
	}

	@Override
	public void run() {

		int x = 0;
		while( x < tries ) {
			try {
				x++;
				Files.move( src, dst, StandardCopyOption.REPLACE_EXISTING );
				break;
			} catch( Exception e ) {
				System.out.println( "N�o conseguiu mover o arquivo <" + src.getFileName()
						+ "> tentativa <" + x + "> de <" + tries + "> " );
				try {
					Thread.sleep( 100 * x  > 10000 ? 10000 : 100 * x);
				} catch( InterruptedException ex ) {
					Thread.currentThread().interrupt();
				}
				// e.printStackTrace();
			}
		}
		if( x < tries ) {
			System.out.println( "Arquivo <" + src.getFileName() + "> movido..." );
		} else {
			System.out.println( "N�o consegui mover o arquivo <" + src.getFileName()
					+ ">. Favor remover manualmente" );
		}
	}
}
