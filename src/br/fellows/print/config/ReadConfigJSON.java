package br.fellows.print.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.fellows.file.exceptions.DiretorioNaoExisteException;

/**
 * Esta classe manipula o acesso ao arquivo de configura��o em formato JSON
 * 
 * Os diret�rios ser�o verificados se existem. Se n�o existirem ser� lan�ada
 * exce��o
 * DiretorioNaoExisteException
 * 
 * Se as configura��es de cria��o de diret�rio n�o est�o marcadas para cria��o o
 * diret�rio n�o existir
 * ser� levantada uma exce��o informando que o diret�rio n�o existe e a execu��o
 * ser� interrompida.
 * 
 * @author andre
 * 
 */
public class ReadConfigJSON<T> {

	private HashMap<String, T>	hm;
	private String							configFileName;

	public ReadConfigJSON( String configFileName ) {
		hm = new HashMap<>();
		this.configFileName = configFileName;
	}

	/**
	 * Inicia a leitura do arquivo de configura��o com as configura��es
	 * de cria��o de pastas, se previamente halbilitadas.
	 */
	public void parse() throws Exception {
		try {
			init();
		} catch( FileNotFoundException e ) {
			throw new Exception( "Arquivo de configura��o <" + this.configFileName + "> n�o encontrado\n" + e.getMessage() );
		} catch( IOException e ) {
			throw new Exception( e.getMessage() );
		} catch( ParseException e ) {
			throw new Exception( "Favor verificar o arquivo de configura��o <"
					+ this.configFileName + ">.\n" + e.getMessage() );
		} catch( DiretorioNaoExisteException e ) {
			throw new Exception( e.getMessage() );
		}
	}

	public HashMap<String, T> getConfigs() {
		return this.hm;
	}

	/**
	 * Se o diret�rio informado n�o existir, ser� lan�ada uma exce��o
	 * 
	 * @param dirname
	 * @throws Exception
	 */
	private void checkDir( String dirname ) throws DiretorioNaoExisteException {
		File theDir = new File( dirname );

		if( !theDir.exists() ) {
			throw new DiretorioNaoExisteException( dirname );
		}
	}

	/**
	 * Se o diret�rio informado n�o existir, o mesmo ser� criado
	 * 
	 * @param dirname
	 */
	private void createDir( String dirname ) {
		File theDir = new File( dirname );

		if( !theDir.exists() ) {
			try {
				Files.createDirectories( Paths.get( dirname ) );
			} catch( IOException e ) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings( "unchecked" )
	private void init() throws Exception {

		JSONParser parser = new JSONParser();
		FileReader fr = new FileReader( this.configFileName );

		JSONArray fileConfs = (JSONArray) parser.parse( fr );
		for( Object confs : fileConfs ) {
			JSONObject conf = (JSONObject) confs;

			String fileNameEnding = (String) conf.get( "FileNameEnding" );
			String folderSrc = (String) conf.get( "FolderSrc" );
			String folderDst = (String) conf.get( "FolderDst" );
			String sCreateSrcFolder = (String) conf.get( "CreateSrcFolder" );
			String sCreateDstFolder = (String) conf.get( "CreateDstFolder" );
			String printer = (String) conf.get( "Printer" );

			String lineSpaceFactor = (String) conf.get( "LineSpaceFactor" );
			String fontSize = (String) conf.get( "FontSize" );
			String style = (String) conf.get( "Style" );
			String family = (String) conf.get( "Family" );

			boolean createSrcFolder = false;
			boolean createDstFolder = false;

			if( sCreateSrcFolder != null ) {
				createSrcFolder = Boolean.parseBoolean( sCreateSrcFolder );
			} else {
				createSrcFolder = false;
			}

			if( sCreateDstFolder != null ) {
				createDstFolder = Boolean.parseBoolean( sCreateDstFolder );
			} else {
				createDstFolder = false;
			}

			if( fileNameEnding == null ) {
				System.out.println(
						"Tag 'FileNameEnding' n�o encontrada. Verifique suas configura��es" );
				System.exit( 1 );
			}
			if( folderSrc == null ) {
				System.out
						.println( "Tag 'FolderSrc' n�o encontrada. Verifique suas configura��es" );
				System.exit( 1 );
			}
			if( folderDst == null ) {
				System.out
						.println( "Tag 'FolderDst' n�o encontrada. Verifique suas configura��es" );
				System.exit( 1 );
			}

			if( printer == null ) {
				System.out
						.println( "Tag 'Printer' n�o encontrada. Verifique suas configura��es" );
				System.exit( 1 );
			}

			if( !folderSrc.endsWith( "/" ) ) {
				folderSrc += "/";
			}

			if( !folderDst.endsWith( "/" ) ) {
				folderDst += "/";
			}

			if( createSrcFolder ) {
				createDir( folderSrc );
			}

			if( createDstFolder ) {
				createDir( folderDst );
			}

			checkDir( folderSrc );
			checkDir( folderDst );

			FileToPrinterConfig cfg = new FileToPrinterConfig();

			cfg.setCreateDstFolder( createDstFolder );
			cfg.setCreateSrcFolder( createSrcFolder );
			cfg.setFileNameEnding( fileNameEnding );
			cfg.setFolderDst( folderDst );
			cfg.setFolderSrc( folderSrc );
			cfg.setPrinter( Integer.parseInt( printer ) );
			if( lineSpaceFactor != null ) {
				cfg.setLineSpaceFactor( Float.parseFloat( lineSpaceFactor ) );
			}
			if( fontSize != null ) {
				cfg.setFontSize( Integer.parseInt( fontSize ) );
			}
			if( style != null ) {
				cfg.setStyle( Integer.parseInt( style ) );
			}
			if( family != null ) {
				cfg.setFamily( family );
			}

			hm.put( fileNameEnding, (T) cfg );

		}
	}
}
