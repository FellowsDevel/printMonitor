package br.fellows.print.config;

public class FileToPrinterConfig {

	private String	fileNameEnding;
	private String	folderSrc;
	private String	folderDst;
	private int			printer;

	private float		lineSpaceFactor;
	private int			fontSize;
	private int			style;
	private String	family;

	private boolean	createSrcFolder;
	private boolean	createDstFolder;

	public FileToPrinterConfig() {
	}

	public float getLineSpaceFactor() {
		return lineSpaceFactor;
	}

	public void setLineSpaceFactor( float lineSpaceFactor ) {
		this.lineSpaceFactor = lineSpaceFactor;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize( int fontSize ) {
		this.fontSize = fontSize;
	}

	public int getStyle() {
		return style;
	}

	public void setStyle( int style ) {
		this.style = style;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily( String family ) {
		this.family = family;
	}

	public void setFileNameEnding( String fileNameEnding ) {
		this.fileNameEnding = fileNameEnding;
	}

	public String getFileNameEnding() {
		return this.fileNameEnding;
	}

	public void setPrinter( int printer ) {
		this.printer = printer;
	}

	public int getPrinter() {
		return this.printer;
	}

	public String getFolderSrc() {
		return folderSrc;
	}

	public void setFolderSrc( String folderSrc ) {
		this.folderSrc = folderSrc;
	}

	public String getFolderDst() {
		return folderDst;
	}

	public void setFolderDst( String folderDst ) {
		this.folderDst = folderDst;
	}

	public boolean isCreateSrcFolder() {
		return createSrcFolder;
	}

	public void setCreateSrcFolder( boolean createSrcFolder ) {
		this.createSrcFolder = createSrcFolder;
	}

	public boolean isCreateDstFolder() {
		return createDstFolder;
	}

	public void setCreateDstFolder( boolean createDstFolder ) {
		this.createDstFolder = createDstFolder;
	}
}
