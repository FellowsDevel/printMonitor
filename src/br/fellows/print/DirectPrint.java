package br.fellows.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.PageRanges;

public class DirectPrint implements Printable {

	private PrintService[]	printService;
	private String					fileToPrint;
	private int							printer						= 0;
	private int							spacing;
	private int							fontSize;
	private int							style;
	private String					family;

	private static int			DEFAULT_SPACING		= 14;
	private static int			DEFAULT_FONTSIZE	= 13;
	private static int			DEFAULT_STYLE			= Font.PLAIN;
	private static String		DEFAULT_FAMILY		= Font.MONOSPACED;

	public DirectPrint() {
		this.printService = PrinterJob.lookupPrintServices();
	}

	public void setPrinter( int printer ) throws Exception {
		if( printer < this.printService.length && printer >= 0 ) {
			this.printer = printer;
		} else {
			throw new Exception(
					"C�digo de impressora incorreto, favor verificar os par�metros" );
		}
	}

	public void setSpacing( int spacing ) {
		this.spacing = spacing;
	}

	public void setFontSize( int fontSize ) {
		this.fontSize = fontSize;
	}

	public void setStyle( int style ) {
		this.style = style;
	}

	public void setFamily( String family ) {
		this.family = family;
	}

	private void validateParameters() {
		if( this.spacing == 0 || this.spacing < 1 ) {
			this.spacing = DEFAULT_SPACING;
		}
		if( this.fontSize == 0 || this.fontSize < 5 ) {
			this.fontSize = DEFAULT_FONTSIZE;
		}
		if( this.style == 0 || ( this.style != Font.BOLD
				&& this.style != Font.ITALIC && this.style != Font.PLAIN ) ) {
			this.style = DEFAULT_STYLE;
		}
		if( this.family == null
				|| ( !this.family.equalsIgnoreCase( Font.MONOSPACED )
						&& !this.family.equalsIgnoreCase( Font.SANS_SERIF )
						&& !this.family.equalsIgnoreCase( Font.SERIF ) ) ) {
			this.family = DEFAULT_FAMILY;
		}
	}

	private int countPages() {
		BufferedReader br = null;
		int pages = 1;
		try {

			br = new BufferedReader( new FileReader( this.fileToPrint ) );

			int lines = 0;
			while( br.readLine() != null ) {
				lines++;
			}
			pages += Math.round( lines / 60 );

		} catch( IOException e ) {
			e.printStackTrace();
		} finally {
			try {
				if( br != null )
					br.close();
			} catch( IOException ex ) {
				// ex.printStackTrace();
			}
		}
		return pages;

	}

	public void printFile( String fileToPrint ) {

		this.fileToPrint = fileToPrint;

		PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
		aset.add( new PageRanges( 1, countPages() ) );
		aset.add( new Copies( 1 ) );

		PrinterJob printJob = PrinterJob.getPrinterJob();
		printJob.setPrintable( this );

		try {
			printJob.setPrintService( printService[ this.printer ] );
			printJob.print( aset );
		} catch( PrinterException err ) {
			System.err.println( err );
		}
	}

	public int print( Graphics g, PageFormat pf, int pageIndex )
			throws PrinterException {
		int x = 1;
		int y = 10;

		validateParameters();

		Graphics2D g2 = (Graphics2D) g;
		g2.translate( pf.getImageableX(), pf.getImageableY() );
		g2.setFont( new Font( this.family, this.style, this.fontSize ) );

		BufferedReader br = null;

		try {
			String sCurrentLine;
			br = new BufferedReader( new FileReader( this.fileToPrint ) );

			while( ( sCurrentLine = br.readLine() ) != null ) {
				System.out.println( sCurrentLine );
				g.drawString( String.valueOf( sCurrentLine ), x, y );
				y += this.spacing;
			}

		} catch( IOException e ) {
			e.printStackTrace();
		} finally {
			try {
				if( br != null )
					br.close();
			} catch( IOException ex ) {
				// ex.printStackTrace();
			}
		}

		return PAGE_EXISTS;
	}
}
