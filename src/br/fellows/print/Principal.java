package br.fellows.print;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import com.zehon.exception.FileTransferException;
import com.zehon.ftp.FTP;

public class Principal {

  private static void createPrinterList() {
    File file = new File( "printers.list" );

    try {
      file.createNewFile();

      FileWriter fw = new FileWriter( file.getAbsoluteFile() );
      BufferedWriter bw = new BufferedWriter( fw );
      PrintService[] printServices = PrintServiceLookup.lookupPrintServices( null, null );

      int x = 0;
      for( PrintService printer : printServices ) {
        bw.write( x++ + " - " + printer.getName() + "\n" );
      }
      bw.close();
    } catch( IOException e ) {
      e.printStackTrace();
    }
  }

  public static void main( String[] args ) throws FileTransferException {
  	for(String fn : FTP.getFileNamesInFolder( "/", "localhost", "andre", "123" )){
  		System.out.println( fn );
  	}
  	
  	FTP.moveFile( "README.txt", "01", "README2.txt", "02", "localhost", "andre", "123" );
  	
    if( args != null ) {
      for( String arg : args ) {
        if( arg.equalsIgnoreCase( "GENERATE_PRINTER_LIST" ) ) {
          System.out.println( "Criando lista de impressoras" );
          createPrinterList();
        }
      }
    }
  }

}
