package br.fellows.print;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.List;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import br.fellows.print.config.FileToPrinterConfig;
import br.fellows.print.config.ReadConfigJSON;

public class FilePrintMonitor {

	private static final String	CONFIG_NAME				= "preferencesConfig.txt";
	private static final String	PRINTERLIST_NAME	= "printerList.txt";

	private static void createPrinterList() {
		File file = new File( PRINTERLIST_NAME );

		try {
			file.createNewFile();

			FileWriter fw = new FileWriter( file.getAbsoluteFile() );
			BufferedWriter bw = new BufferedWriter( fw );
			PrintService[] printServices = PrintServiceLookup.lookupPrintServices( null, null );

			int x = 0;
			for( PrintService printer : printServices ) {
				String s = x++ + " - " + printer.getName() + "\n";
				bw.write( s );
				System.out.println( s );
			}
			bw.close();
		} catch( IOException e ) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings( "rawtypes" )
	public static void main( String[] args ) throws Exception {

		if( args != null ) {
			for( String arg : args ) {
				if( arg.equalsIgnoreCase( "GENERATE_PRINTER_LIST" ) ) {
					System.out.println( "Criando lista de impressoras" );
					createPrinterList();
					System.exit( 0 );
				}
			}
		}

		ReadConfigJSON<FileToPrinterConfig> jsonFtP = new ReadConfigJSON<FileToPrinterConfig>(
				CONFIG_NAME );

		jsonFtP.parse();
		HashMap<String, FileToPrinterConfig> configurations = jsonFtP.getConfigs();

		WatchService watchService = FileSystems.getDefault().newWatchService();

		if( configurations == null ) {
			System.out.println(
					"Verifique a exist�ncia do arquivo de configura��o 'preferences.config'\n\n" );
			System.exit( 1 );
		}

		for( FileToPrinterConfig fc : configurations.values() ) {
			Path folderSrc = Paths.get( fc.getFolderSrc() );
			configurations.put( fc.getFileNameEnding(), fc );
			folderSrc.register( watchService, ENTRY_CREATE, ENTRY_MODIFY );
		}

		for( ;; ) {

			System.out.println( "Aguardando arquivo..." );
			WatchKey watchKey;
			try {
				watchKey = watchService.take();

				List<WatchEvent<?>> events = watchKey.pollEvents();

				for( WatchEvent watchEvent : events ) {

					WatchEvent.Kind kind = watchEvent.kind();

					if( kind == OVERFLOW ) {
						System.out.println( "OVERFLOW!!!!" );
						continue;
					}

					// Nome do evento que aconteceu.
					String eventKindName = kind.name();
					// System.out.println( "Nome do evento: " + eventKindName );

					// Pega o nome do arquivo que disparou o evento
					String fileName = watchEvent.context().toString();
					String fileNameEnding = "";

					for( FileToPrinterConfig ftpc : configurations.values() ) {
						fileNameEnding = ftpc.getFileNameEnding();
						if( fileName != null && fileNameEnding != null
								&& fileName.substring( 0, fileName.lastIndexOf( "." ) )
										.endsWith( fileNameEnding ) ) {
							break;
						}
						fileNameEnding = null;
					}
					if( fileNameEnding == null ) {
						continue;
					}

					// System.out.println( "Arquivo: " + fileName );
					// System.out.println( "pasta: " + watched );
					// System.out.println( " " );

					// Verifica se o arquivo est� na pasta de origem
					File f = new File(
							configurations.get( fileNameEnding ).getFolderSrc() + fileName );
					if( !f.exists() && !f.isDirectory() ) {} else {
						if( eventKindName.equals( "ENTRY_MODIFY" ) ) {
							System.out.println( "Processando arquivo <" + fileName + ">." );
							doAction( configurations.get( fileNameEnding ), fileName );
						}
					}
				}
				if( !watchKey.reset() ) {
					break;
				}
			} catch( Exception e ) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		watchService.close();
	}

	private static void doAction( FileToPrinterConfig fc, String fileName )
			throws Exception {

		Path src = Paths.get( fc.getFolderSrc() + fileName );
		Path dst = Paths.get( fc.getFolderDst() + fileName );

		PageableText pt = new PageableText( new File( fc.getFolderSrc() + fileName ) );
		pt.setFamily( fc.getFamily() );
		pt.setFontSize( fc.getFontSize() );
		pt.setPrinter( fc.getPrinter() );
		pt.setLineSpaceSeparator( fc.getLineSpaceFactor() );
		pt.print();

		FileMoveThread fmt = new FileMoveThread( src, dst );
		fmt.run();

		// DirectPrint dp = new DirectPrint();
		// dp.setPrinter( fc.getPrinter() );
		// dp.setFamily( fc.getFamily() );
		// dp.setFontSize( fc.getFontSize() );
		// dp.setSpacing( fc.getSpacing() );
		// dp.setStyle( fc.getStyle() );
		// dp.printFile( fc.getFolderSrc() + fileName );

		// try {
		// Thread.sleep( 100 );
		// } catch( InterruptedException ex ) {
		// Thread.currentThread().interrupt();
		// }

		// while( true ) {
		// try {
		// Files.move( src, dst, StandardCopyOption.REPLACE_EXISTING );
		// break;
		// } catch( IOException e ) {
		// //e.printStackTrace();
		// }
		// }
		// System.out.println( "Arquivo <" + src.getFileName() + "> movido..." );
	}

}
