package br.fellows.file.exceptions;

public class DiretorioNaoExisteException extends Exception {

	private static final long serialVersionUID = 7775312422783024284L;

	public DiretorioNaoExisteException() {
		super( "O diret�rio informado n�o existe" );
	}

	public DiretorioNaoExisteException( String dirName ) {
		super( "O diret�rio <" + dirName + "> n�o existe" );
	}

}
