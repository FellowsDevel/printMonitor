Pr�-requisito:
	O Java vers�o 1.8 (talvez 1.7 funcione, n�o testei)


Este programa tem como objetivo monitorar diret�rios definidos em "FolderSrc" para que 
quando os arquivos de TEXTO com nome terminado com o padr�o definido na configura��o 
"FileNameEnding", o mesmo ser� impresso na impressora configurada em "Printer".

Ap�s a impress�o o arquivo ser� MOVIDO para o diret�rio definido em "FolderDst".

� necess�rio gerar o arquivo que lista os c�digos das impressoras instaladas no sistema 
e este arquivo � somente para visualiza��o dos c�digos correspondentes a cada impressora.

Para gerar o arquivo siga as seguintes etapas:
	1-abra o prompt de comando do DOS
	2-acesse o diret�rio onde ficar� os arquivos(ex: C:\Print)
	3-execute o seguinte comando sem as aspas "java -jar filePrintMonitor.jar GENERATE_PRINTER_LIST"

Ser� criado no mesmo diret�rio um arquivo com o nome "printerList.txt" e nele existir� a lista 
de c�digos seguidos de suas respectivas descri��es (nome das impressoras). 
O c�digo servir� para configurar o "preferencesConfig.txt".


� necess�rio alterar a configura��o do arquivo "preferencesConfig.txt" seguindo as seguintes regras:

	O arquivo de configura��o tem nota��o JSON.
	O conjunto de configura��es s�o cercadas por [] que indica um array.
	Cada configura��o s�o cercadas por {} que indica um objeto e entre objetos, � necess�rio usar uma v�rgula.

	Todas as "tags" s�o obrigat�rias, exceto as "tags" "Spacing, FontSize, Style e Family".
	Todas as "tags" e valores DEVEM obedecer a grafia CamelCase conforme descrito no exemplo e DEVEM estar 
	dentro de Aspas Duplas (").

Descri��o das tags:

- FileNameEnding: 
	Termina��o do nome do arquivo para monitorar. 

- FolderSrc: 
	Caminho do diret�rio onde os arquivos ser�o disponibilizados. Para maior compatibilidade, 
	dever� ser utilizado a nota��o de caminho tipo URI.
	Podem ser utilizados caminhos absolutos ou relativos.
	No Windows � poss�vel utilizar a nota��o normal do sistema devendo-se observar que 
	as barras \\ devem ser duplas.
	As configura�oes a seguir produzem o mesmo efeito:
		"c:\\temp\\diretorio_origem"
	 	"c:\\temp\\diretorio_origem\\"
	 	"/temp/diretorio_origem"
	 	"/temp/diretorio_origem/"

- FolderDst: 
	Caminho do diret�rio para onde o arquivo disponibilizado no diret�rio FolderSrc ser� movido 
	ap�s a impress�o.
	Podem ser utilizados caminhos absolutos ou relativos.
	No Windows � poss�vel utilizar a nota��o normal do sistema devendo-se observar que 
	as barras \\ devem ser duplas.
	As configura�oes a seguir produzem o mesmo efeito:
		"c:\\temp\\diretorio_destino"
	 	"c:\\temp\\diretorio_destino\\"
	 	"/temp/diretorio_destino"
	 	"/temp/diretorio_destino/"

- CreateSrcFolder: 
	Informa se o diret�rio de origem dever� ser criado no momento de execu��o do programa.
	Valores v�lidos: "true" ou "false"
	Se o diret�rio n�o existir e esta configura��o estiver em "false", o programa ser� abortado 
	com uma exce��o

- CreateDstFolder: 
	Informa se o diret�rio de destino dever� ser criado no momento de execu��o do programa.
	Valores v�lidos: "true" ou "false"
	Se o diret�rio n�o existir e esta configura��o estiver em "false", o programa ser� abortado 
	com uma exce��o

- Printer: 
	C�digo da impressora a ser utilizada.
	O c�digo estar� dispon�vel no arquivo printersList.txt gerado inicialmente

- Spacing:
	Inteiro que define o espa�amento entre linhas.
	Se n�o informado, o tamanho ser� 14 (pontos)

- FontSize:
	Inteiro que define o tamanho da Fonte.
	Se n�o informado, o tamanho ser� 13.

- Style:
	Inteiro que define o estilo da Fonte. 
	Valores v�lidos: 
		0 - PLAIN
		1 - BOLD
		2 - ITALIC
	Se n�o informado, o estilo "PLAIN" ser� utilizado

- Family:
	String que define a fam�lia da Fonte.
	Valores v�lidos: "Monospaced", "SansSerif" ou "Serif"
	Se n�o informado, o tipo "Monospaced" ser� utilizado
		
Para executar o programa siga as seguintes etapas:
	1-abra o prompt de comando do DOS
	2-acesse o diret�rio onde ficar� os arquivos do programa(ex: C:\Print)
	3-execute o seguinte comando sem as aspas "java -jar filePrintMonitor.jar"


Qualquer d�vida ou sugest�o, favor entrar em contato atrav�s do email: fellows.devel@gmail.com